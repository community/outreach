# Tor Bilgilendirme Başlıkları 2019-2020

# 1. KİŞİSEL GİZLİLİK İÇİN TOR

### Kişisel gizlilik bir insan hakkıdır

Aleisha çoğumuz gibi zamanının önemli bir kısmında çevrimiçi oluyor. Arkadaşları ile görüşüyor, sosyal ağlarda paylaşım yapıyor ve İnternet sitelerine bakıyor.

Bir süre önce, çevrimiçi olduğu yerlerde daha önce yaptığı aramalar ile ilgili reklamlar görmeye başladığını farketti.

Bu durumu rahatsız edici bularak çevrimiçi reklamlar konusunu araştırdığında, yalnız reklamcıların değil, İnternet hizmeti sağlayıcıları, istatistik ve çevrimiçi davranış toplama firmaları, sosyal ağ platformları gibi pek çok kuruluşun kendisini izlediğini öğrendi.

Aleisha kişisel bilgilerini toplamayan, çevrimiçi işlemlerini izlemeyen ve onun hakkında özel olan herhangi bir bilgiyi başka yerlere aktarmayan bir yazılım bulmak ve kullanmak istediğine karar verdi. 

Bunun üzerine katıldığı bir kişisel bilgileri koruma eğitiminde **Tor Browser** uygulamasının anonim kalarak işlem yapmayı sağlayan tek tarayıcı olduğunu öğrendi.

---

# 2. FEMİNİSTLER İÇİN TOR

### Gelecek siberfeminist olacak

Fernanda kürtajın yasa dışı olduğu Brezilya'da bir kadın birliği ile üreme hakları üzerinde çalışıyor.

Fernanda ve arkadaşları kürtaj olanakları, doğum kontrolü ve üreme ile ilgili diğer bilgileri içeren bir web sitesi oluşturmuş.

Bu web sitesi ile bağlantılı oldukları anlaşılırsa tutuklanabilirler ya da başlarına daha kötü şeyler gelebilir.

Kendilerini korumak için Fernanda ve arkadaşları web sitesini Tor **onion hizmetleri** üzerinde hazırladılar. Onion hizmetleri hem onların web sitesini hazırlayanlar olarak bulunabilmelerini engelliyor hem de web sitesine erişmek için Tor Browser kullanması gerektiğinden ziyaretçilerini de korumuş oluyor.

Aslında Fernanda güvenli tarafta kalabilmek için tüm İnternet aramalarını yalnız **Tor Browser** ile yapıyor.

Ayrıca çeşitli dosyaları diğer aktivistler ile güvenli bir şekilde paylaşırken kişisel gizliliğini de korumak için Tor tarafından geliştirilmiş **OnionShare** uygulamasını kullanıyor.

### Tor, üreme hakkı için mücadele eden Fernanda gibi temel hakları savunan aktivistlere yardımcı olur.

---

# 3. İNSAN HAKLARI İÇİN TOR

### Su hayattır

Jelanı içinden büyük bir nehir geçen küçük bir köyde yaşıyor.

Bu nehir atalarının zamanından beri köy halkının suyunu sağlıyor.

Ancak bugün Jelani'nin nehri bölgede petrol sondajı yapan çok uluslu güçlü şirketlerin tehdidi altında.

Bu şirketler için çalışan özel güvenlik firmaları, nehirlerini korumak için bir araya gelen Jelani ve komşularından oluşan köy halkının çevrimiçi yaptıklarını izlemek için gelişmiş yöntemler kullanıyor.

Jelani uluslararası insan hakları ya da hukuk ile ilgili web sitelerini ziyaret etmek ve köyündeki direniş hakkında yazılar yazmak için **Tor Browser** kullanıyor.

Ayrıca elindeki belgeleri insan haklarını açığa çıkarmak üzerine çalışan gazetecilere güvenli bir şekilde iletmek için **OnionShare** ve **SecureDrop** uygulamalarını kullanıyor. 

Jelani'nin kişisel gizliliğin korunmasına yardımcı olan tüm bu uygulamalar Tor kullanır. 

### Jelani gibi insan hakları aktivistleri toplumlarında adaleti sağlamak için çalışırlarken Tor onlara yardımcı olur.

---

# 4. SANSÜRE KARŞI TOR

### Duvarlar değil köprüler kurun

Jean ilk kez ailesinden uzakta bir ülkeye gidiyor.

Oteline vardığında bilgisayarını açıyor.

Web tarayıcısında "Bağlantı zaman aşımına uğradı" iletisini görünce önce bunun kendi hatasından olduğunu düşünüyor.

Ancak bir kaç kez yeniden denediğinde e-posta hizmeti sağlayıcısına, bir haber sitesine giremediğini ve pek çok uygulamayı kullanamadığını fark ediyor.

Bu ülkede İnternet erişimine sansür uygulandığını duyduğunu anımsayınca sorunun bu olduğunu anlıyor.
Aşılmaz bir duvarın arkasından ailesi ile nasıl görüşebileceğini merak ediyor.
Web üzerinde biraz araştırma yapınca bir forumda sansürlenmeyen bir ağ üzerine bağlanmayı sağlayan VPN adında özel hizmetler bulunduğunu öğreniyor. 

Jean en ucuz ve iyi VPN hizmetinin hangisi olduğunu anlamak için bir buçuk saat harcıyor.

İçlerinden birini seçiyor ve bu bir süre için işe yarıyor gibi görünüyor. Ancak beş dakika sonra bağlantı kopuyor ve bir daha VPN bağlantısı kurulamıyor. 

Jean başka ne yapabileceğini araştırıyor ve Tor Browser uygulaması ile sansürü nasıl aşabileceğini öğreniyor.

Uygulamayı indirebileceği bir resmi yansı web sitesi buluyor.

**Tor Browser** uygulamasını başlattıktan sonra sansür uygulanan kullanıcılara uygun ayarları seçerek bir köprüye bağlanıyor ve İnternet'e yeniden erişebiliyor.

Jean Tor Browser kullanarak kişisel gizliliğinin korunmasını sağlarken İnternet'e özgürce bağlanıp ailesi ile görüşebiliyor.

### Dünyada sansüre uğrayan tüm kullanıcılar Tor Browser kullanarak ücretsiz ve güvenli bir şekilde İnternet'e erişebilir.

---

# 5. PAYLAŞILAN BÖLÜMLER

## Tor nedir?

Tor çevrimiçi izlenmenizi engelleyen ve sansürü aşmanızı sağlayan özgür ve ücretsiz bir açık ağdır.
Tor Birleşik Devletler 501(c)3 koşulları altında faaliyet gösteren ve kar amacı gütmeyen bir kuruluş olan Tor Projesi tarafından geliştirilmiştir.

Tor kullanmanın en kolay yolu Tor Browser uygulamasıdır.
Tor Browser uygulamasını kullandığınızda hiç kimse hangi web sitelerini ziyaret ettiğinizi ve dünyanın neresinde olduğunuzu göremez.

SecureDrop ve OnionShare gibi diğer uygulamalar kullanıcılarını izleme ve sansüre karşı korur.


## 6. TOR NASIL ÇALIŞIR?

Amal, kişisel gizliliğini koruyarak Bekele'nin web sitesine bakmak ister ve Tor Browser uygulamasını açar.

Tor Browser, tüm dünya üzerinde Tor ağının trafiğini yönlendirmek üzere ayarlanmış pek çok bilgisayardan rastgele üç tanesini seçer ve bir aktarıcı devresi oluşturur. 

Ardından Tor Browser, web sitesi isteğini üç kez şifreler ve az önce oluşturulmuş olan Tor devresindeki ilk aktarıcıya gönderir.

İlk aktarıcı birinci şifreleme katmanını çözer ancak hedefin Bekele'nin web sitesi olduğunu öğrenemez.

Birinci aktarıcı yalnız devrede bir sonraki hedefin ikinci aktarıcı olduğunu öğrenir.

İkinci aktarıcı ikinci şifreleme katmanını çözer ve hedefi öğrenerek web sayfası isteğini üçüncü aktarıcıya iletir.

Üçüncü aktarıcı son şifreleme katmanını çözer ve web sayfasını isteğini Bekele'nin web sitesine iletir. Ancak bu isteğin Amal tarafından yapıldığını bilmez.

Bekele, Amal kendisine söylemedikçe web sitesi ile ilgili isteğin ondan geldiğini bilemez.

## 7. KİMLER TOR KULLANIR?

Tüm dünyadan insanlar kişisel gizliliklerini korumak ve web sitelerine özgürce erişmek için Tor kullanır.

Tor gazetecilere, insan hakları savunucularına, yerel şiddet kurbanlarına, akademik araştırmacılara ve izleme, sansür ya da gözetimle karşılaşan herkese yardımcı olur.

## 6. TOR'A NEDEN GÜVENEYİM?

Tor kişisel gizliliği korumak için tasarlanmıştır. Kullanıcılarımızın kim olduğunu bilmeyiz ve kullanıcıların işlemleri ile ilgili herhangi bir günlük kaydı tutmayız. 

Tor aktarıcı hizmeti sunanlar, aktarıcılarını kullanan Tor kullanıcılarının gerçek kimliklerini öğrenemez.

Tor kaynak kodunun akademik ve açık kaynak toplulukları tarafından sürekli gözden geçirilmesi sayesinde Tor üzerinde herhangi bir arka kapı bulunmaz. Ayrıca topluma Tor üzerinde bir arka kapı bulunmayacağına dair verdiğimiz bir söz var.

## 7. TOR TOPLULUĞUNA KATILIN

Tor, dünyanın dört bir yanındaki kullanıcılar, geliştiriciler, aktarıcı işletmecileri ve destekçiler sayesinde var oluyor.

İsteyen herkes için Tor uygulamasını daha kullanışlı ve güvenli hale getirmek için desteğiniz gerekiyor.

Kod geliştirerek, bir aktarıcı işleterek, belgeler yazarak, kullanıcı desteği sunarak ya da çevrenizdekileri Tor hakkında bilgilendirerek Tor projesine gönüllü destek verebilirsiniz.

Tor topluluğunun davranış kuralları vardır ve topluluğa verdiğimiz sözleri toplumsal sözleşmemizde özetliyoruz.

Web sitemizi ve wiki sayfalarımızı ziyaret ederek, IRC üzerinde bizi bularak, e-posta listelerimizden birine katılarak ya da newsletter.torproject.org adresinden Tor News duyurularına abone olarak Tor hakkında daha fazla bilgi alabilirsiniz.


## 8. TOR UYGULAMASINI İNDİRİN

Masaüstü için Tor
torproject.org/download

MOBİL AYGIT ÜZERİNDE TOR
### Android 
Android için Tor Browser, GooglePlay üzerinden alınabilir.

### iOS
M. Tigas tarafından geliştirilen Onion Browser, iOS için önerdiğimiz tek tarayıcıdır.

