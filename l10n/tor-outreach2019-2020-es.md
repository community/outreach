# Material de divulgación de Tor 2019-2020

# 1. TOR PARA LA PRIVACIDAD

### La privacidad es un derecho humano.

Como muchos de nosotros, Aleisha pasa la mayor parte de su tiempo en línea: conectada con los amigos, publicando en redes sociales y navegando por la web.

Pero últimamente, ha notado que los anuncios relacionados con sus búsquedas anteriores la siguen doquiera que navegue.

Esto resulta tan invasivo que investiga sobre anuncios en la red y aprende que no solo los anunciantes la rastrean, sino también su ISP, las compañías de análisis, las plataformas de redes sociales y más.

Aleisha decide que quiere encontrar y usar software que no recopile sus datos, no la rastree y no informe a otros servicios nada privado sobre ella.

Asiste a un curso sobre privacidad en un espacio de hackers local y aprende sobre el **Tor Browser **, el único navegador web que le permite navegar de forma anónima.

---

# 2.TOR PARA LOS Y LAS FEMINISTAS 

### El futuro es ciberfeminista

Fernanda dirige un colectivo de mujeres centrado en los derechos reproductivos en Brasil, donde el aborto es ilegal.

Fernanda y sus compañeras crearon un sitio web con información sobre el acceso al aborto, el control de la natalidad y otros recursos para las personas que buscan información reproductiva.

Si se vinculara este sitio web a ellas, las podrían arrestar, o peor.

Para protegerse, Fernanda y sus colegas crearon el sitio web utilizando los **servicios cebolla  de Tor**. Los servicios cebolla no sólo las protegen de ser descubiertas como operadoras del servidor, sino que también ayudan a proteger a los visitantes de su sitio web al requerir que usen el Tor Browser.

De hecho, Fernanda utiliza el **Tor Browser** para toda su navegación en la web solo para estar segura.

Ella también usa una aplicación de Tor llamada **OnionShare** para enviar archivos a otras activistas de forma segura y privada.

### Las activistas por los derechos reproductivos como Fernanda luchan por libertades fundamentales, y Tor ayuda a fortalecer su resistencia.

---

# 3. TOR POR LOS DERECHOS HUMANOS

### El agua es vida

Jelani vive en un pequeño pueblo atravesado por un ancho río.

Este río ha proporcionado agua a su comunidad desde los más remotos tiempos.

Pero ahora, el río de Jelani se ve amenazado por poderosas compañías multinacionales que perforan buscando petróleo en la región.

Las empresas de seguridad privadas, pagadas por estas compañías, utilizan poderosos mecanismos de vigilancia para monitorear las actividades en línea de Jelani y sus vecinos de la aldea, que se están organizando para proteger su río sagrado.

Jelani usa el **Tor Browser** para evitar que estas compañías lo vean mientras visita sitios web de protección de derechos humanos y asistencia legal internacionales y escribe en blogs sobre el movimiento de resistencia en su aldea.

También usa **OnionShare** y **SecureDrop** para enviar documentos de manera segura a periodistas que están ayudando a hacer público estas violaciones de derechos humanos.

Tor usa todo este software para ayudar a proteger la privacidad de Jelani.

### Los activistas por los derechos humanos como Jelani luchan por la justicia en sus comunidades, y Tor ayuda a fortalecer su resistencia.

---

# 4. TOR LUCHA CONTRA LA CENSURA

### Construye puentes, no muros

Jean estaba viajando por primera vez a un país lejos de su familia.

Después de llegar al hotel, abrió el portátil.

Estaba tan agotado que la primera vez que apareció en su navegador el mensaje "La conexión se ha agotado", pensó que era un error suyo.

Tras intentarlo una y otra vez, se dio cuenta de que su proveedor de correo electrónico, un sitio web de noticias y muchas aplicaciones no estaban disponibles.

Había oído que en este país se censuraba a internet y se preguntaba si era eso lo que sucedía.
¿Cómo podría ponerse en contacto con su familia con ese muro impenetrable?
Después de hacer algunas búsquedas en la red, encontró un foro y leyó sobre VPN, servicios privados que te permiten conectarte a otra red sin censura.

Jean se pasó media hora tratando de averiguar qué VPN barata sería mejor.

Eligió uno y en un principio parecía que funcionaba, pero después de cinco minutos, la conexión se desconectó y la VPN ya no se conectó.

Jean siguió leyendo buscando otras opciones y aprendió sobre el Tor Browser y sobre cómo puede eludir la censura.

Encontró un mirror oficial para bajarse el programa.

Cuando abrió el **Tor Browser**, siguió las indicaciones para usuarios censurados y se conectó a un puente que le permitió acceder a Internet nuevamente. 

Con Tor, Jean puede navegar libre y privadamente y contactar con su familia.

### Los usuarios censurados de todo el mundo confían en el Tor Browser para conseguir una forma libre, estable y sin censura de acceder a Internet.

---

# 5. Secciones compartidas

## ¿Qué es Tor?

Tor es software libre y una red abierta que te ayuda a protegerte contra el seguimiento, la vigilancia y la censura en línea.
Tor es creada de forma libre por una organización sin ánimo de lucro 501 (c) 3 con sede en los Estados Unidos llamada el Proyecto Tor.

La forma más fácil de usar Tor es el Tor Browser.
Cuando usas el Tor Browser, nadie puede ver qué sitios web visitas o de qué parte del mundo vienes

Otras aplicaciones, como SecureDrop y OnionShare, usan Tor para proteger a sus usuarios contra la vigilancia y la censura.


## 6. ¿Como funciona Tor?

Amal quiere visitar la web de Bekele en privado, así que abre el Tor Browser.

El Tor Browser selecciona un circuito aleatorio de tres relays, que son ordenadores de todo el mundo configurados para enrutar el tráfico a través de la red Tor.

El Tor Browser a continuación encripta la solicitud del sitio web tres veces y la envía al primer relay Tor del circuito.

El primer relay elimina la primera capa de encriptación pero no se entera de que el destino es la web Bekele.

El primer repetidor solo sabe la siguiente ubicación en el circuito, que es el segundo repetidor.

El segundo repetidor elimina otra capa de cifrado y envía la solicitud de página web al tercer repetidor.

El tercer repetidor elimina la última capa de cifrado y envía la solicitud de la web a su destino, el sitio web Bekele, pero no sabe que la solicitud proviene de Amal.

Bekele no sabe que la solicitud proviene de Amal a menos que se lo diga ella.

## 7. ¿Quién usa Tor? 

Personas de todo el mundo usan Tor para proteger su privacidad y acceder a la web libremente.

Tor ayuda a proteger a periodistas, defensores de los derechos humanos, víctimas de violencia doméstica, investigadores académicos y cualquier persona que sufra el rastreo, la censura o la vigilancia.

## 6. ¿Por qué confiar en Tor?

Tor está diseñado para la privacidad. No sabemos quiénes son nuestros usuarios y no mantenemos registros de la actividad de los usuarios.

Los operadores de retransmisión de Tor no pueden revelar la verdadera identidad de los usuarios de Tor.

La revisión continua del código fuente de Tor por parte de las comunidades académicas y de código abierto garantiza que no haya puertas traseras en Tor, y nuestro contrato social promete que nunca lo haremos.

## 7. Ingresa en la comunidad Tor

Tor es posible gracias a un conjunto diverso de usuarios, desarrolladores, operadores de retransmisión y defensores de todo el mundo.

Necesitamos tu ayuda para hacer que Tor sea más sencillo y seguro para las personas de todo el mundo.

Puedes ser voluntario de Tor escribiendo código, manteniendo un repetidor, creando documentación, ofreciendo soporte al usuario o difundiendo entre las personas de tu comunidad qué es Tor.

La comunidad de Tor se rige por un código de conducta, y mostramos nuestro compromiso con la comunidad en nuestro contrato social.

Consigue más información sobre Tor visitando nuestro sitio web, nuestro wiki, encontrándonos en IRC, uniéndote a una de nuestras listas de correo o inscribiéndote en Tor News en newsletter.torproject.org.


## 8. Descargar Tor

Tor para escritorio
torproject.org/download

TOR EN EL MÓVIL
### Android 
El Tor Browser para Android está disponible en GooglePlay.

### iOS
El Onion Browser, desarrollado por M. Tigas, es el único navegador que recomendamos para iOS.

