# Material de mobilizare 2019-2020

# 1. TOR PENTRU CONFIDENȚIALITATE

### Confidențialitatea este un drept uman

Ca mulți dintre noi, Aleisha își petrece cea mai mare parte a timpului online - conectându-se cu prietenii, postând pe social media și navigând pe web.

Dar, în ultima vreme, a observat că reclamele legate de căutările ei anterioare o urmăresc online.

Aceasta se simte atât de invazivă încât face cercetări privind anunțurile online și află că nu numai agenții de publicitate o urmăresc, ci și ISP-ul, companiile de analiză, platformele sociale media și multe altele.

Aleisha decide că dorește să găsească și să utilizeze software care nu colectează datele ei, nu o urmărește și nu spune altor servicii nimic privat despre ea.

Ea merge la un training de confidențialitate dintr-un spațiu hackers local și află despre ** Tor Browser **, singurul navigator web care îi permite să navigheze anonim.

---

# 2.TOR PENTRU FEMINIȘTI 

### Viitorul este cyberfeminist

Fernanda conduce un colectiv de femei axat pe drepturile reproductive în Brazilia, unde avortul este ilegal.

Fernanda și colegii ei au construit un site web cu informații despre accesul la avort, controlul nașterilor și alte resurse pentru persoanele care caută informații despre reproducere.

Dacă prin acest site se poate ajunge la ei, ar putea fi arestați - sau mai rău.

Pentru a se proteja, Fernanda și colegii ei au creat site-ul web folosind serviciile ** Tor **. Serviciile Onion nu numai că îi protejează de a fi descoperiți ca operatori ai serverului, ci ajută, de asemenea, la protejarea vizitatorilor pe site-ul lor web prin solicitarea utilizării Tor Browser.

De fapt, Fernanda folosește **Tor Browser** pentru toate browserele web doar pentru a fi pe partea sigură.

De asemenea, ea utilizează o aplicație Tor-powered, numită **OnionShare**, pentru a trimite în siguranță și în mod privat fișiere altor activiști.

### Activiștii pentru drepturile reproductive, precum Fernanda, se luptă pentru libertățile fundamentale, iar Tor îi ajută în activitatea de rezistență.

---

# 3. TOR PENTRU DREPTURILE OMULUI

### Apa este viață

Jelani trăiește într-un mic sat prin care trece un râu larg.

Acest râu a furnizat apă comunității sale din moși-strămoși.

Dar astăzi, râul lui Jelani este amenințat de puternice companii multinaționale care sondează petrolul din regiune.

Firmele private de securitate, plătite de aceste companii, utilizează mecanisme puternice de supraveghere pentru a monitoriza activitățile online ale lui Jelani și a vecinilor săi din sat care se organizează pentru a-și proteja râul sacru.

Jelani folosește **Tor Browser** pentru a împiedica aceste companii să vizioneze site-ul pentru protecția drepturilor omului și a asistenței juridice internaționale și scrie bloguri despre mișcarea de rezistență din satul său.

De asemenea, el folosește **OnionShare** și **SecureDrop** pentru a trimite în siguranță documente către jurnaliștii care ajută la expunerea acestor încălcări ale drepturilor omului.

Toate aceste programe folosesc Tor pentru a proteja intimitatea lui Jelani.

### Activiștii pentru drepturile omului, cum ar fi Jelani, se luptă pentru justiție în comunitățile lor, iar Tor îi ajută la activitatea de rezistență.

---

# 4. TOR PENTRU ANTI-CENZURĂ

### Construiți poduri, nu ziduri

Jean călătorea pentru prima dată într-o țară departe de familia sa.

După ce a ajuns la un hotel, și-a deschis laptopul.

S-a simțit nedumerit atunci când a apărut pentru prima oară pe browserul său web mesajul "Connection has timed out" și a crezut că mesajul s-a datorat propriei erori.

Dar după ce a încercat de mai multe ori, și-a dat seama că furnizorul de e-mail, un site de știri și multe aplicații nu erau disponibile.

A auzit că în această țară se cenzurează internetul și se întreba dacă se întâmplă acest lucru.
Cum putea să-și contacteze familia din spatele acestui zid impenetrabil?
După ce a făcut niște căutări pe web, a găsit un forum și a citit despre VPN-uri, servicii private care vă permit să vă conectați la o altă rețea necenzurată.

Jean a petrecut o jumătate de oră încercând să-și dea seama care VPN ieftin era cel mai bun.

El a ales unul și, pentru un timp, părea că funcționează, dar după cinci minute conexiunea a ieșit offline și VPN-ul nu se mai putea conecta.

Jean continua să citească pentru a găsi alte opțiuni și a învățat despre Tor Browser și cum poate eluda cenzura.

A găsit un site web oficial pentru a descărca programul.

A deschis browserul **Tor Browser**, a urmat instrucțiunile pentru utilizatorii cenzurați și s-a conectat la un pod /bridge care i-a permis din nou accesul la internet.

Cu Tor Browser, Jean poate naviga liber și privat și poate lua legătura cu familia sa.

### Utilizatorii cenzurați din întreaga lume se bazează pe Tor Browser pentru o modalitate gratuită, stabilă și necenzurată de a accesa internetul.

---

# 5. Secțiuni comune

## Ce e Torul?

Tor este un software gratuit și o rețea deschisă care te ajută să te protejezi de urmărire, supraveghere și cenzură online.
Tor este creat gratuit de către o organizație nonprofit 501(c)3 din S.U.A., numită Tor Project.

Cea mai ușoară modalitate de a utiliza Tor este Tor Browser.
Atunci când utilizați Tor Browser, nimeni nu poate vedea ce site-uri Web vizitați sau din ce locație le vizitați.

Alte aplicații, cum ar fi SecureDrop și OnionShare, utilizează Tor pentru a-și proteja utilizatorii împotriva supravegherii și cenzurii.


## 6. Cum funcționează Tor?

Amal dorește să viziteze privat site-ul Bekele, așa că deschide browserul Tor.

Tor Browser selectează un circuit aleatoriu de trei relee, care sunt computere din întreaga lume configurate pentru a ruta traficul prin rețeaua Tor.

Tor Browser criptează de trei ori cererea ei pentru website și o trimite la primul releu Tor în circuitul ei.

Primul releu elimină primul strat de criptare, dar nu află că destinația este site-ul Bekele.

Primul releu află doar următoarea locație din circuit, care este al doilea releu.

Al doilea releu elimină un alt strat de criptare și înaintează cererea de pagină web către cel de-al treilea releu.

Al treilea releu elimină ultimul strat de criptare și înaintează cererea de pagină web către destinația sa, site-ul Bekele, dar nu știe că cererea vine de la Amal.

Bekele nu știe că cererea pentru website a venit de la Amal, cu excepția cazului în care ea i-ar fi spus asta.

## 7. Cine folosește Tor? 

Oamenii din întreaga lume folosesc Tor pentru a-și proteja intimitatea și pentru a accesa liber internetul.

Tor ajută la protejarea jurnaliștilor, a apărătorilor drepturilor omului, a victimelor violenței domestice, a cercetătorilor universitari și a oricui se confruntă cu urmărirea, cenzura sau supravegherea.

## 6. De ce să aveți încredere în Tor?

Tor este proiectat pentru intimitate. Nu știm cine sunt utilizatorii noștri și nu ținem jurnalele de activitate ale utilizatorilor.

Operatorii releului Tor nu pot dezvălui adevărata identitate a utilizatorilor Tor.

Evaluarea continuă a codului sursă Tor de către comunitățile academice și cu surse deschise garantează că nu există backdoors în Tor, iar contractul nostru social promite că backdoor nu va fi folosit niciodată în Tor.

## 7. Aderați la comunitatea Tor

Realizarea Tor a fost posibilă datorită mai multor utilizatori, dezvoltatori, operatori de retransmisie și avocați din întreaga lume.

Avem nevoie de ajutorul dvs. pentru ca Tor să devină mai ușor de folosit și mai sigur pentru oamenii de pretutindeni.

Puteți contribui voluntar la Tor, scriind un cod, rulând un releu, creând documentație, oferind suport pentru utilizatori sau spunând oamenilor din comunitatea dvs. despre Tor.

Comunitatea Tor este guvernată de un cod de conduită și subliniem promisiunile noastre către comunitate din contractul nostru social.

Aflați mai multe despre Tor vizitând site-ul nostru, pagina noastră wiki, găsindu-ne pe IRC, alăturându-vă uneia dintre listele noastre de discuții sau înscriindu-vă la Tor News de pe newsletter.torproject.org.


## 8. Descărcare Tor

Tor pentru Desktop
torproject.org/download

TOR PE MOBIL
### Android 
Tor Browser pentru Android este valabil pe GooglePlay.

### iOS
Onion Browser, dezvoltat de M. Tigas, este unicul browser pe care îl recomandăm pentru iOS.

