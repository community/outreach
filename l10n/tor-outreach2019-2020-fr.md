# Documents de sensibilisation à Tor 2019-2020

# 1. TOR POUR LE RESPECT DE LA VIE PRIVÉE

### La vie privée est un droit de la personne

Comme beaucoup d’entre nous, Aleisha passe la plupart de son temps en ligne, à communiquer avec des amis, à publier sur les médias sociaux et à parcourir le Web.

Mais dernièrement, elle a remarqué que des publicités liées à ses recherches précédentes la suivent partout en ligne.

Elle trouve cela si envahissant qu’elle fait des recherches sur les publicités en ligne et apprend que ce n’est pas seulement les annonceurs qui la suivent à la trace, mais aussi son FAI, les entreprises d’analyse, les plateformes de médias sociaux et d’autres.

Aleisha décide qu’elle veut trouver et utiliser des logiciels qui ne recueillent pas ses données, ne la suivent pas à la trace et ne révèlent rien de confidentiel à son sujet à d’autres services.

Elle suit dans un laboratoire numérique ouvert local une formation sur la protection de la vie privée et découvre le **navigateur Tor Browser**, le seul navigateur Web qui lui permet de naviguer de façon anonyme.

---

# 2. TOR POUR LES FÉMINISTES

### L’avenir est cyberféministe

Fernanda dirige un collectif de femmes axé sur les droits liés à la procréation au Brésil, où l’avortement est illégal.

Fernanda et ses collègues ont créé un site Web qui offre des renseignements sur l’accès à l’avortement, la régulation des naissances et d’autres ressources pour les personnes à la recherche de renseignements sur la procréation.

Si l’on pouvait remonter la trace de ce site Web jusqu’à elles, elles pourraient être arrêtées ou pire encore.

Pour se protéger, Fernanda et ses collègues ont créé le site Web en utilisant les **services onion** de Tor. Non seulement les services onion empêchent qu’elles soient découvertes comme opératrices du serveur, mais ils aident également à protéger les visiteurs de leur site Web en exigeant qu’ils utilisent le navigateur Tor Browser.

En fait, Fernanda utilise le **navigateur Tor Browser** pour toute sa navigation sur le Web, simplement par mesure de sécurité.

Elle utilise également une application propulsée par Tor appelée **OnionShare** pour envoyer des fichiers à d’autres activistes en toute sécurité et en toute confidentialité.

### Les défenseures des droits liés à la procréation, comme Fernanda, luttent pour les libertés fondamentales et Tor aide à propulser leur résistance.

---

# 3. TOR POUR LES DROITS DE LA PERSONNE

### L’eau, c’est la vie

Jelani vit dans un petit village traversé par une large rivière.

Cette rivière fournit de l’eau à sa communauté depuis l’époque de ses ancêtres.

Mais aujourd’hui, la rivière de Jelani est menacée par de puissantes multinationales qui forent des puits de pétrole dans la région.

Les entreprises de sécurité privées, financées par ces entreprises, utilisent de puissants mécanismes pour surveiller les activités en ligne de Jelani et de ses voisins dans le village qui s’organisent pour protéger leur rivière sacrée.

Jelani utilise le **navigateur Tor Browser** pour empêcher ces entreprises de le surveiller alors qu’il visite des sites Web sur la protection internationale des droits de la personne et sur l’aide juridique, et rédige des articles de blogue sur le mouvement de résistance dans son village.

Il utilise également **OnionShare** et **SecureDrop** pour envoyer en toute sécurité des documents aux journalistes qui contribuent à exposer ces violations des droits de la personne.

Tous ces logiciels utilisent Tor pour protéger la vie privée et les renseignements personnels de Jelani.

### Des défenseurs des droits de la personne comme Jelani luttent pour la justice dans leurs communautés et Tor aide à propulser leur résistance.

---

# 4. TOR POUR L’ANTICENSURE

### Bâtir des ponts plutôt que de dresser des murs

Jean voyageait pour la première fois dans un pays loin de sa famille.

Une fois arrivé à l’hôtel, il a ouvert son ordinateur portable.

Il était tellement épuisé que lorsque le message « La connexion est expirée » est apparu pour la première fois sur son navigateur Web, il a pensé qu’il avait fait une erreur.

Mais après avoir essayé encore et encore, il s’est rendu compte qu’il ne pouvait plus accéder à son fournisseur de services de courriel ni à un site Web de nouvelles ni à beaucoup d’applis.

Il avait entendu dire que ce pays censure Internet et se demanda si c’était le cas.
Comment pouvait-il contacter sa famille derrière ce mur impénétrable ?
Après avoir fait quelques recherches sur le Web, il a trouvé un forum et a découvert les RPV, des services privés et confidentiels qui vous permettent de vous connecter à un autre réseau non censuré.

Jean passa une demi-heure à essayer de déterminer quel RPV bon marché était le meilleur.

Il en choisit un et pendant un moment il sembla fonctionner, mais après cinq minutes, la connexion tomba hors ligne et le RPV ne se connectait plus.

Jean continua à lire pour trouver d’autres options et découvrit le navigateur Tor Browser et la façon dont il contourne la censure.

Il trouva un miroir officiel du site Web pour télécharger le programme.

Quand il ouvrit le **navigateur Tor Browser**, il suivit les indications pour les utilisateurs censurés et se connecta à un pont qui lui permit à nouveau d’accéder à Internet.

Avec le navigateur Tor Browser, Jean peut naviguer librement, en toute confidentialité, et contacter sa famille.

### Dans le monde entier, des utilisateurs censurés comptent sur le navigateur Tor Browser pour une façon gratuite, stable et non censurée d’accéder à Internet.

---

# 5. Sections partagées

## Qu’est-ce que Tor ?

Tor est un logiciel gratuit et un réseau ouvert qui aident à vous protéger contre le suivi à trace, la surveillance et la censure en ligne.
Tor est créé gratuitement par un organisme à but non lucratif 501(c)3 situé aux États-Unis appelée Le Projet Tor.

La façon la plus simple d’utiliser Tor est avec le navigateur Tor Browser.
Quand vous utilisez le navigateur Tor Browser, personne ne peut savoir quels sites Web vous visitez ni d’où, dans le monde, vous venez. 

D’autres applications telles que SecureDrop et OnionShare utilisent Tor pour protéger leurs utilisateurs contre la surveillance et la censure.


## 6. Comment Tor fonctionne-t-il ?

Amal veut visiter le site Web de Bekele en toute confidentialité, alors elle ouvre le navigateur Tor Browser.

Le navigateur Tor Browser sélectionne un circuit aléatoire de trois relais, qui sont des ordinateurs partout dans le monde configurés pour acheminer le trafic sur le réseau Tor.

Le navigateur Tor Browser chiffre alors la requête de son site Web trois fois et l’envoie au premier relais Tor dans son circuit.

Le premier relais supprime la première couche de chiffrement, mais n’apprend pas que la destination est le site Web de Bekele.

Le premier relais n’apprend que le prochain emplacement du circuit, qui est le deuxième relais.

Le deuxième relais supprime une autre couche de chiffrement et achemine la requête de page Web au troisième relais.

Le troisième relais supprime la dernière couche de chiffrement et achemine la demande de page Web à sa destination, le site Web de Bekele, mais il ne sait pas que la demande provient d’Amal.

Bekele ne sait pas que la requête de site Web provenait d’Amal, sauf si elle le lui dit.

## 7. Qui utilise Tor ?

Dans le monde entier, des personnes utilisent Tor pour protéger leur vie privée et leurs renseignements personnels, mais aussi accéder librement au Web.

Tor aide à protéger les journalistes, les défenseurs des droits de la personne, les victimes de violence familiale, les chercheurs universitaires et toute personne confrontée au pistage, à la censure ou à la surveillance.

## 6. Pourquoi faire confiance à Tor ?

Tor est conçu pour la confidentialité. Nous ne savons pas qui nos utilisateurs sont ni ne gardons de journaux de l’activité des utilisateurs.

Les opérateurs de relais Tor ne peuvent pas divulguer la véritable identité des utilisateurs de Tor.

L’examen collégial continu du code source de Tor par les communautés universitaires et par celles des logiciels à code source ouvert garantit qu’il n’y a pas de portes dérobées dans Tor. De plus, notre contrat social implique que nous n’introduiront jamais de portes dérobées dans Tor.

## 7. Se joindre à la communauté de Tor

Tor est rendu possible grâce à un ensemble varié d’utilisateurs, de développeurs, d’opérateurs de relais et de défenseurs du droit à la vie privée partout dans le monde.

Nous avons besoin de votre aide pour rendre Tor plus facile à utiliser et plus sûr, pour tous, partout.

Vous pouvez faire du bénévolat auprès de Tor en écrivant du code, en exploitant un relais, en rédigeant de la documentation, en offrant de l’assistance aux utilisateurs ou encore en parlant de Tor aux gens de votre communauté.

La communauté de Tor est régie par un code de conduite et nous énonçons l’ensemble de nos promesses à la communauté dans notre contrat social.

Apprenez-en davantage au sujet de Tor en visitant notre site Web, notre wiki, en nous trouvant sur IRC, en vous joignant à l’une de nos listes de diffusion, ou encore en vous inscrivant aux nouvelles de Tor sur newsletter.torproject.org.


## 8. Télécharger Tor

Tor pour ordinateur
torproject.org/download

TOR SUR APPAREIL MOBILE
### Android
Le navigateur Tor Browser pour Android est proposée sur Google Play.

### iOS
Le Navigateur Onion, développé par M. Tigas, est le seul navigateur que nous recommandons pour iOS.

