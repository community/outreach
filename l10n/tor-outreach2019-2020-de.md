# Tor Outreach Material 2019-2020

# 1. TOR FÜR PRIVATSPHÄRE 

### Datenschutz ist ein Menschenrecht

Wie viele von uns verbringt Aleisha die meiste Zeit mit Online-Kontakten mit Freunden, Postings in sozialen Medien und dem Surfen im Internet.

Aber in letzter Zeit hat sie bemerkt, dass Anzeigen, die sich auf ihre früheren Recherchen beziehen, sie online verfolgen.

Dies fühlt sich so aufdringlich an, dass sie einige Recherchen über Online-Anzeigen durchführt und erfährt, dass es nicht nur Werbetreibende sind, die sie verfolgen, sondern auch ihr ISP, Analytikunternehmen, Social Media Plattformen und mehr.

Aleisha beschließt, Software zu finden und zu verwenden, die ihre Daten nicht sammelt, sie nicht verfolgt und anderen Diensten nichts Privates über sie erzählt.

Sie besucht ein Privattraining in einem lokalen Hackerspace und lernt **Tor Browser** kennen, den einzigen Webbrowser, der es ihr ermöglicht, anonym zu surfen.

---

# 2.TOR FÜR FEMINISTINNEN 

### Die Zukunft ist cyberfeministisch.

Fernanda leitet ein Frauenkollektiv, das sich auf Reproduktionsrechte in Brasilien konzentriert, wo Abtreibung illegal ist.

Fernanda und ihre Kollegen erstellten eine Website mit Informationen über Abtreibungszugang, Geburtenkontrolle und andere Ressourcen für Menschen, die reproduktive Informationen suchen.

Wenn diese Website mit ihnen verlinkt wäre, könnten sie verhaftet werden - oder schlimmer noch.

Um sich selbst zu schützen, haben Fernanda und ihre Kollegen die Website mit den Tor **onion-Diensten** erstellt. Onion-Dienste schützen sie nicht nur davor, als Betreiber des Servers entdeckt zu werden, sondern helfen auch, Besucher ihrer Website zu schützen, indem sie verlangen, dass sie Tor-Browser verwenden. 

Tatsächlich verwendet Fernanda **Tor Browser** für alle ihre Webbrowser, nur um auf der sicheren Seite zu sein.

Sie verwendet auch eine Tor-powered App namens **OnionShare**, um Dateien sicher und privat an andere Aktivisten zu senden.

### Aktivisten für Reproduktionsrechte wie Fernanda kämpfen für die Grundfreiheiten, und Tor hilft, ihren Widerstand zu stärken.

---

# 3. TOR FÜR MENSCHENRECHTE

### Wasser ist Leben 

Jelani lebt in einem kleinen Dorf, durch das ein breiter Fluss fließt.

Dieser Fluss versorgt seine Gemeinde seit den Tagen seiner Vorfahren mit Wasser.

Aber heute ist Jelani's Fluss von mächtigen multinationalen Unternehmen bedroht, die in der Region nach Öl bohren.

Private Sicherheitsfirmen, die von diesen Unternehmen bezahlt werden, nutzen leistungsstarke Überwachungsmechanismen, um die Online-Aktivitäten von Jelani und seinen Nachbarn im Dorf zu überwachen, die sich für den Schutz ihres heiligen Flusses einsetzen.

Jelani verwendet **Tor Browser**, um zu verhindern, dass diese Unternehmen zusehen, wie er Websites zum internationalen Schutz der Menschenrechte und zur Rechtshilfe besucht und Blogbeiträge über die Widerstandsbewegung in seinem Dorf schreibt.

Er verwendet auch **OnionShare** und **SecureDrop**, um Dokumente sicher an Journalisten zu senden, die helfen, diese Menschenrechtsverletzungen aufzudecken.

Alle diese Programme verwenden Tor, um die Privatsphäre von Jelani zu schützen.

### Menschenrechtsaktivisten wie Jelani kämpfen für Gerechtigkeit in ihren Gemeinden, und Tor hilft, ihren Widerstand zu stärken.

---

# 4. TOR FÜR ANTI-ZENSUR

### Brücken bauen statt Mauern

Jean reiste zum ersten Mal in ein Land, das weit von seiner Familie entfernt war.

Als er in einem Hotel ankam, öffnete er seinen Laptop.

Er war so erschöpft, dass er, als die Meldung "Connection has timed out" zum ersten Mal in seinem Webbrowser erschien, dachte, es sei auf seinen eigenen Fehler zurückzuführen.

Aber nachdem er es immer wieder versucht hatte, erkannte er, dass sein E-Mail-Provider, eine Nachrichten-Website und viele Apps nicht verfügbar waren.

Er hatte gehört, dass dieses Land das Internet zensiert und sich gefragt, ob das passiert.
Wie konnte er seine Familie von hinter dieser undurchdringlichen Mauer aus kontaktieren?
Nachdem er einige Websuchen durchgeführt hatte, fand er ein Forum und las über VPNs, private Dienste, die es Ihnen ermöglichen, sich mit einem anderen unzensierten Netzwerk zu verbinden.

Jean verbrachte eine halbe Stunde damit, herauszufinden, welches preisgünstige VPN das Beste ist.

Er wählte eines aus, und für einen Moment schien es zu funktionieren, aber nach fünf Minuten ging die Verbindung offline, und das VPN wollte sich nicht mehr verbinden.

Jean las weiter, um andere Optionen zu finden, und lernte Tor Browser kennen und wie er die Zensur umgehen kann.

Er fand einen offiziellen Website-Mirror, um das Programm herunterzuladen.

Als er **Tor Browser** öffnete, folgte er den Anweisungen für zensierte Benutzer und verband sich mit einer Brücke, die es ihm ermöglichte, wieder auf das Internet zuzugreifen.

Mit Tor Browser kann Jean frei und privat surfen und seine Familie kontaktieren.

### Zensierte Benutzer auf der ganzen Welt verlassen sich auf Tor Browser für einen kostenlosen, stabilen und unzensierten Zugang zum Internet.

---

# 5. Gemeinsame Bereiche

## Was ist Tor?

Tor ist kostenlose Software und ein offenes Netzwerk, das dich vor Verfolgung, Überwachung und Zensur online schützt.
Tor wird kostenlos von einem 501(c)3 U.S.-amerikanischen gemeinnützigen Verein namens Tor Project erstellt.

Der einfachste Weg, Tor zu benutzen, ist der Tor-Browser.
Wenn du Tor-Browser verwendest, kann niemand sehen, welche Websites du besuchst oder von wo in der Welt du kommst. 

Andere Anwendungen, wie SecureDrop und OnionShare, verwenden Tor, um ihre Benutzer vor Überwachung und Zensur zu schützen.


## 6. Wie funktioniert Tor?

Amal will Bekeles Website privat besuchen, also öffnet sie den Tor-Browser.

Der Tor-Browser wählt einen zufälligen Schaltkreis aus drei Relays aus, d.h. Computern auf der ganzen Welt, die konfiguriert sind, um den Verkehr über das Tor-Netzwerk zu leiten.

Tor Browser verschlüsselt dann dreimal ihre Website-Anfrage und sendet sie an das erste Tor-Relay in ihrer Schaltung.

Das erste Relay entfernt die erste Verschlüsselungsebene, erfährt aber nicht, dass das Ziel die Website von Bekele ist.

Das erste Relay erfährt nur den nächsten Ort in der Schaltung, nämlich das zweite Relay.

Das zweite Relay entfernt eine weitere Verschlüsselungsebene und leitet die Webseitenanforderung an das dritte Relay weiter.

Das dritte Relay entfernt die letzte Verschlüsselungsebene und leitet die Webseitenanfrage an ihr Ziel, Bekeles Website, weiter, aber es weiß nicht, dass die Anfrage von Amal kommt.

Bekele weiß nicht, dass die Website-Anfrage von Amal kam, es sei denn, sie sagt es ihm.

## 7. Wer benutzt Tor?

Menschen auf der ganzen Welt nutzen Tor, um ihre Privatsphäre zu schützen und frei auf das Internet zuzugreifen.

Tor hilft Journalisten, Menschenrechtsaktivisten, Opfern häuslicher Gewalt, akademischen Forschern und allen, die Verfolgung, Zensur oder Überwachung erleben.

## 6. Warum Tor vertrauen?

Tor ist auf Privatsphäre ausgelegt. Wir wissen nicht, wer unsere Benutzer sind, und wir führen keine Protokolle über die Benutzeraktivität.

Tor-Relay-Betreiber können die wahre Identität von Tor-Benutzern nicht preisgeben.

Kontinuierliche Peer-Reviews des Quellcodes von akademischen und Open-Source-Communities stellen sicher, dass es in Tor keine Hintertüren gibt, und unser Sozialvertrag verspricht, dass wir nie hinter die Kulissen von Tor gehen werden.

## 7. Der Tor-Community beitreten

Tor wird durch eine Vielzahl von Benutzern, Entwicklern, Relaybetreibern und Anwälten aus der ganzen Welt ermöglicht.

Wir brauchen deine Hilfe, um Tor benutzerfreundlicher und sicherer für alle Menschen zu machen.

Du kannst dich freiwillig bei Tor melden, indem du Code schreibst, einen Relay betreibst, Dokumentation erstellst, Benutzerunterstützung anbietest oder Leuten in deiner Community von Tor erzählst.

Die Tor-Community unterliegt einem Verhaltenskodex, und wir beschreiben unsere Versprechen an die Community in unserem Gesellschaftsvertrag.

Erfahren Sie mehr über Tor, indem Sie unsere Website, unser Wiki besuchen, uns im IRC finden, einer unserer Mailinglisten beitreten oder sich für Tor News anmelden unter newsletter.torproject.org.


## 8. Tor herunterladen

Tor für Desktop
torproject.org/download

TOR AUF DEM HANDY
### Android
Tor-Browser für Android ist bei GooglePlay erhältlich.

### iOS
Onion Browser, entwickelt von M. Tigas, ist der einzige Browser, den wir für iOS empfehlen.

